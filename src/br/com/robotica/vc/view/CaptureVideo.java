package br.com.robotica.vc.view;

import java.awt.FlowLayout;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import br.com.robotica.vc.arduino.SerialCOM;
import br.com.robotica.vc.core.ProcessaImagem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CaptureVideo {

	private static String portArduino = "COM7"; 
	private static Boolean processaVideo = Boolean.FALSE;

	
	static {
		// Loading the OpenCV core library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	

	public static void main(String[] args) throws IOException {
		
		
		//Apply the Styles
		//Nimbus, Windows and Metal
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            //Catching Multiple Exception (Multi-catch)
        	System.err.println(ex);
        }
		
		

		// start capturing the camera
		VideoCapture video = new VideoCapture(0);

		
		
		// Create a frame with the actual image
		MyFrame frameReal = new MyFrame();
		frameReal.setVisible(true);
		frameReal.setCustomTitle("Real Image");

		
		
		// Create a frame with the custom image
		MyFrame frame = new MyFrame();
		frame.setVisible(true);
		frame.setCustomTitle("Custom Image");
		frame.setCustomLocation(690, 0);

		
		
		// JFrame which holds JPanel
		JFrame frameButtons = new JFrame();
		frameButtons.getContentPane().setLayout(new FlowLayout());

		MyPanel panel = new MyPanel();
		frameButtons.getContentPane().add(panel);

		JButton goButton = new JButton("Go");
		JButton backButton = new JButton("Back");
		
		goButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				
				try
				{
					
					SerialCOM serial = new SerialCOM();
					serial.processArduino(portArduino, "1");
					
					//Tempo de rota��o do servo (para entrada do biscoito)
				    Thread.sleep(4000);
				}
				catch(InterruptedException ex)
				{
				    Thread.currentThread().interrupt();
				}
				
				
				processaVideo = Boolean.TRUE;

				
				
			}
			
		});

		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				SerialCOM serial = new SerialCOM();
				serial.processArduino(portArduino, "0");
				
				processaVideo = Boolean.FALSE;
				
			}
		});

		frameButtons.getContentPane().add(goButton);
		frameButtons.getContentPane().add(backButton);

		frameButtons.setVisible(true);
		frameButtons.setTitle("Buttons");
		frameButtons.setSize(250, 100);
		frameButtons.setLocation(50, 550);
		frameButtons.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Mat mat = new Mat();
		Mat matResult = new Mat();

		
		
		if (video.isOpened()) {

			while (true) {
				video.read(mat);
				video.read(matResult);

				matResult = ProcessaImagem.processImage(mat);

				frame.render(matResult);
				frameReal.render(mat);

				MatOfFloat ranges = new MatOfFloat(0f, 256f);
				MatOfInt histSize = new MatOfInt(25);
				Mat hist_1 = new Mat();
				Imgproc.calcHist(Arrays.asList(matResult), new MatOfInt(0),
						new Mat(), hist_1, histSize, ranges);
				
				
				if(processaVideo){
					
					toArray(matResult);
			
					processaVideo = Boolean.FALSE;
				}

			}
			
			

		} else {
			
			System.out.println("Camera not detected \n");

			String title = "Error";
			String text = "Camera not detected";
			int input = JOptionPane.showConfirmDialog(null, text, title,
					JOptionPane.DEFAULT_OPTION);

			if (input == JOptionPane.OK_OPTION) {
				System.out.println("Exit application");

				// Exit application
				System.exit(0);
			}

		}


	}

	
	
	
	public static void toArray(Mat m) {

		if (processaVideo) {

			// Transfer bytes from Mat to BufferedImage
			int bufferSize = m.channels() * m.cols() * m.rows();
			byte[] b = new byte[bufferSize];
			
			// get all the pixels
			m.get(0, 0, b); 

			int cont0 = 0;
			int cont1 = 0;

			for (int i = 0; i < b.length; i++) {
				if (b[i] == 0) {
					cont0++;
				} else {
					cont1++;
				}
			}
			
			
			System.out.println("Preto 0 > " + cont0 + " Branco -1 > " + cont1);
			
			
			//5% de margem de erro
			
			//Biscoito Bom -> Entre 94000 e 110000
			//Biscoito Ruim -> Entre 1000 e 94000
			//Sem biscoito -> Abaixo de 1000

			if (cont1 >= 94000 && cont1 <= 110000) {
				System.out.println("Biscoito bom");
				JOptionPane.showMessageDialog(null, "Biscoito bom");
			} else {
				
				if(cont1 < 94000 && cont1 > 100) {
					System.out.println("Biscoito ruim");
					JOptionPane.showMessageDialog(null, "Biscoito ruim");
				} else if(cont1 < 1000) {
					System.out.println("Sem biscoito");
					JOptionPane.showMessageDialog(null, "Sem biscoito");
				}
				
			}


		} else {
			System.out.println("processaVideo: " + processaVideo);

		}

	}

}