package br.com.robotica.vc.core;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import org.opencv.core.Mat;

import br.com.robotica.vc.view.MyPanel;



public class AccessImage {
	
	private MyPanel panel;
	
	static int blackPixelsCount = 0;
	static int whitePixelsCount = 0;
	
	
		
	
	public void render(BufferedImage image) {
		panel.setImage(image);
		panel.repaint();
		
	}

	
	
	public MyPanel render(Mat image) {
		
		MyPanel panelRender = new MyPanel();
		
		Image i = toBufferedImage(image);
		panelRender.setImage(i);
		panelRender.repaint();
		
		return panelRender;
		
	}

	
	
	public static Image toBufferedImage(Mat m) {
		// Code from
		// http://stackoverflow.com/questions/15670933/opencv-java-load-image-to-gui

		// Check if image is gray scale or color
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if (m.channels() > 1) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}

		// Transfer bytes from Mat to BufferedImage
		int bufferSize = m.channels() * m.cols() * m.rows();
		byte[] b = new byte[bufferSize];
		m.get(0, 0, b); // get all the pixels
		BufferedImage image = new BufferedImage(100, 100, type);
		if (m.cols() > 0) {
			image = new BufferedImage(m.cols(), m.rows(), type);
		}

		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);
		return image;
	}
	
	
	
	public static void toArray(Mat m) {
		

		// Transfer bytes from Mat to BufferedImage
		int bufferSize = m.channels() * m.cols() * m.rows();
		byte[] b = new byte[bufferSize];
		m.get(0, 0, b); // get all the pixels
		
		
		for(int i = 0; i < b.length; i++){
			
			if(b[i]==0){
				blackPixelsCount++;
				
			}else{
				whitePixelsCount++;
				
			}	
			
		}
		
		
		
		/*
		if(cont1 > 145000 && cont1 < 160000){
			JOptionPane.showMessageDialog(null, "Biscoito bom");
		} else {
			JOptionPane.showMessageDialog(null, "Biscoito ruim");
		}
		
		
		
		*/
		//Fazer uma media de 5 biscoitos bons e colocar 5% de margem de erro
		
		
		System.out.println("Preto '0' = " + blackPixelsCount + "    " + "Branco '-1' = " + whitePixelsCount);
		
		
		
	}
	
	
	public static int getBlackPixelsCount() {
	     return blackPixelsCount;
	}
	

	public static int getWhitePixelsCount() {
	     return whitePixelsCount;
	}

	

}
