package br.com.robotica.vc.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class ProcessaImagem{
	
	static {
		// Loading the OpenCV core library
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	
	
	
	public static Mat imageToGray(String pathFile) throws IOException{
		
		Mat source = Imgcodecs.imread(pathFile);

		Mat destination = new Mat();
		
		Imgproc.cvtColor(source, destination, Imgproc.COLOR_RGB2GRAY);
		
		return destination;
		
	}
	
	
	
	public static Mat imageToBlackWhite(Mat source) throws IOException{
		
		Mat destination = new Mat();
		
		Imgproc.threshold(source, destination, 180.0, 255.0, Imgproc.THRESH_BINARY_INV);
		
		return destination;
		
	}
	
	
	
	public static Mat processImage(Mat source) throws IOException{

		// Creating an empty matrix to store the result
		Mat destination = new Mat();
		
		
		try
		{
		
			//Creating a kernel matrix
			Mat kernel1 = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new  Size(3, 3));
			
			
			//Colored Images to GrayScale
			Imgproc.cvtColor(source, destination, Imgproc.COLOR_RGB2GRAY);
			
			
			// Applying Morphological and Blur effect on the Image to eliminate noise and have a more uniform image
			Imgproc.morphologyEx(destination, destination, Imgproc.MORPH_ERODE, kernel1);
			Imgproc.medianBlur(destination, destination, 15);
			
			
			// Converting to binary image...
			//THRESH_BINARY = background White, THRESH_BINARY_INV = background Black
			Imgproc.threshold(destination, destination, 120.0, 255.0, Imgproc.THRESH_BINARY);
			
			
			
			//Apply Erosion filter
			int erosion_size = 5;
			Mat element  = Imgproc.getStructuringElement(
			    Imgproc.MORPH_CROSS, new Size(2 * erosion_size + 1, 2 * erosion_size + 1), 
			    new Point(erosion_size, erosion_size)
			);
			Imgproc.erode(destination, destination, element);
			
			if(search(source, destination) == true) {
				
			} 
			
		}
		catch (Exception e)
		{
			 System.out.print(e.getMessage());
			 System.out.print("\n");
		}

				
		return destination;
		
	}
	
	
	
	public static Mat countObject(String pathFile) throws IOException{		
		
		Mat originalMat = Imgcodecs.imread(pathFile);
		Mat grayMat = new Mat();
	    Mat cannyEdges = new Mat();
	    	   	    
	    Imgproc.cvtColor(originalMat, grayMat, Imgproc.COLOR_BGR2GRAY);
	    	    
	    Imgproc.Canny(grayMat, cannyEdges, 50, 180);
	   	
		return cannyEdges;
		
	}
	
	
	
	
	
	
	
	private static boolean search(Mat frame, Mat maskedImage)
	{
		
		boolean hasContours = false;
	
		// initialize
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();

		
		// find contours
		Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

		
		// if any contour exist...
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0)
		{
			hasContours = true;
			//System.out.print("Contour: " + hasContours + "\n");

			// for each contour, display it in blue
			for (int idx = 0; idx >= 0; idx = (int) hierarchy.get(0, idx)[0])
			{
				Imgproc.drawContours(
					frame,  						//matrix object of the image
		            contours, 						//p1
		            idx, 							//contour IDX
		            new Scalar(0, 0, 255), 			//color contour
		            2, 								//Line Type
		            Imgproc.LINE_4, 				//Thickness of the line
		            hierarchy						//hierarchy MAT
		        );
		                
		    }
			
			
		}
		
		return hasContours;
		

	}
	
	
}

