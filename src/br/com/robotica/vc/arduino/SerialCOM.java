package br.com.robotica.vc.arduino;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class SerialCOM {

	static SerialPort serialPort;	
	
	
	
	
	public void processArduino(String COMPort, String param) {
		serialPort = new SerialPort(COMPort);
		
		try {
			
			//String[] portNames = SerialPortList.getPortNames();
		    //for(int i = 0; i < portNames.length; i++){
		      //  System.out.println(portNames[i]);
		    //}
			
	        serialPort.openPort();//Open serial port
	        serialPort.setParams(SerialPort.BAUDRATE_9600, 
	                             SerialPort.DATABITS_8,
	                             SerialPort.STOPBITS_1,
	                             SerialPort.PARITY_NONE);//Set params. Also you can set params by this string: serialPort.setParams(9600, 8, 1, 0);
	        
	        serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | 
                    SerialPort.FLOWCONTROL_RTSCTS_OUT);

	        serialPort.addEventListener(new PortReader(), SerialPort.MASK_RXCHAR);
	        
	        Thread.sleep(2000);
	        
	        serialPort.writeString(param); //1 = ASCII 49
	        
	        
	        System.out.println("Enviado");
	    }
	    catch (Exception ex) {
	        System.out.println(ex);
	    }

	}
	
	
	
	
	
	
	private static class PortReader implements SerialPortEventListener {

	    @Override
	    public void serialEvent(SerialPortEvent event) {
	        if(event.isRXCHAR() && event.getEventValue() > 0) {
	            try {
	                String receivedData = serialPort.readString(event.getEventValue());
	                System.out.println("Received response: " + receivedData);
	                serialPort.closePort();//Close serial port
	            }
	            catch (SerialPortException ex) {
	                System.out.println("Error in receiving string from COM-port: " + ex);
	            }
	        }
	    }

	}

}
