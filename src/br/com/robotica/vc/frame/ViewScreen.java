package br.com.robotica.vc.frame;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import br.com.robotica.vc.arduino.SerialCOM;
import br.com.robotica.vc.core.AccessImage;
import br.com.robotica.vc.core.ProcessaImagem;
import br.com.robotica.vc.view.MyPanel;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SpringLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import java.awt.GraphicsEnvironment;

public class ViewScreen extends JFrame {
	

	private static final long serialVersionUID = 1L;
	
	public static VideoCapture video ;
	

	public JPanel contentPane;
	
	public static JPanel normalPanel;
	public static JPanel customPanel;

	public static JLabel labelCookieStatus;
	public static JLabel countPixelsLabel;
	
	
	
	
	static {
		//Loading the OpenCV core library
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	

	
	
	//Launch the application.	
	public static void main(String[] args) {
		
		//start capturing the camera 
		video = new VideoCapture(0);
		
		
		
		//Apply the Styles
		//Nimbus, Windows and Metal
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            //Catching Multiple Exception (Multi-catch)
        	System.err.println(ex);
        }
		
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					
					// Create a new ViewScreen to show
					ViewScreen frame = new ViewScreen();
							
					frame.setTitle("Coockie Detect");
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
					frame.setMaximizedBounds(env.getMaximumWindowBounds());
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					//frame.setUndecorated(true);
							
					frame.setVisible(true);
					
					
					
					if (video.isOpened()){
						Mat mat = new Mat();
						Mat matResult = new Mat();

						
						/*
						String valor_enviado= "1";
						SerialCOM serial = new SerialCOM();
						serial.processArduino("COM6", valor_enviado);
						*/
						
						
						AccessImage imageFrameAI = new AccessImage();
						

					
						
					
							video.read(mat);
							video.read(matResult);
							
							matResult = ProcessaImagem.processImage(mat);
							
							
							normalPanel.add(imageFrameAI.render(mat));
							normalPanel.repaint();  
							
							customPanel.add(imageFrameAI.render(matResult));
							customPanel.repaint();  
							
						
							
							
							
							
							MatOfFloat ranges = new MatOfFloat(0f, 256f);
							MatOfInt histSize = new MatOfInt(25);
							Mat hist_1 = new Mat();
							Imgproc.calcHist(Arrays.asList(matResult), new MatOfInt(0), new Mat(), hist_1, histSize, ranges);
							AccessImage.toArray(matResult);
							
							
						
							
						
							
							
							
						} else {
							System.out.println("Camera not detected");

						    String title = "Error";
							String text = "Camera not detected";
							int input = JOptionPane.showConfirmDialog(
											null, 
											text, 
											title, 
											JOptionPane.DEFAULT_OPTION
										);
					        
							if(input == JOptionPane.OK_OPTION)
							{
								System.out.println("Exit application");
								
								//Exit application
								System.exit(0);
							}
							
						}
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
		
	}
	
	
	
	
	
	
	
	

	//Create the frame
	public ViewScreen() {
		
		setBounds(100, 100, 485, 409);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		
		
		
		
		customPanel = new JPanel();
		
		
		
		
		
		
		normalPanel = new JPanel();
		
		
		
		

		countPixelsLabel = new JLabel("Pixels: " + "Pretos = " + AccessImage.getBlackPixelsCount() + "    " + "Brancos = " + AccessImage.getWhitePixelsCount());

		
		JLabel labelStatus = new JLabel("Status:");
		labelStatus.setFont(new Font("Tahoma", Font.BOLD, 28));
		
		labelCookieStatus = new JLabel("Status do Biscoito");
		labelCookieStatus.setFont(new Font("Tahoma", Font.PLAIN, 24));
		
		
		
		
		//Buttons
		final JButton goButtonTreadmill = new JButton("Go Treadmill");
		final JButton backButtonTreadmill = new JButton("Back Treadmill");
		
		
		
		goButtonTreadmill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				goButtonTreadmill.setEnabled(false);
				backButtonTreadmill.setEnabled(true);
				
				String valor_enviado= "1";
				SerialCOM serial = new SerialCOM();
				serial.processArduino("COM6", valor_enviado);
				
			}
		});
		
		backButtonTreadmill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				goButtonTreadmill.setEnabled(true);
				backButtonTreadmill.setEnabled(false);
				
				String valor_enviado= "0";
				SerialCOM serial = new SerialCOM();
				serial.processArduino("COM6", valor_enviado);
				
			}
		});
		
		
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(24)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(labelStatus, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(labelCookieStatus, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE)
								.addComponent(countPixelsLabel, Alignment.LEADING)))
						.addComponent(normalPanel, GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(goButtonTreadmill, GroupLayout.PREFERRED_SIZE, 90, Short.MAX_VALUE)
							.addGap(18)
							.addComponent(backButtonTreadmill, GroupLayout.PREFERRED_SIZE, 93, Short.MAX_VALUE)
							.addContainerGap())
						.addComponent(customPanel, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)))
		);
		
		
		
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(customPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(normalPanel, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED, 89, Short.MAX_VALUE)
					.addComponent(countPixelsLabel)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelStatus, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(labelCookieStatus, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
							.addGap(28))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(backButtonTreadmill, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
								.addComponent(goButtonTreadmill, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE))
							.addContainerGap())))
		);
		
		
		contentPane.setLayout(gl_contentPane);
	}
}
